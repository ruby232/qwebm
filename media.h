#ifndef MEDIA_H
#define MEDIA_H

#include "converter/mediaconverter.h"
#include "sqlite/bdsqlite.h"

class Media: public QObject
{
    Q_OBJECT

    MediaConverter *converter;
    int id;
    QString source;
    QString destination;
    QString error;
    int state;
    QString extension;

public:
    Media(QString source,QString destination,QString error,int state=Media::waiting);
    Media(int id);
    Media(QString source,QString folder_output="");
    Media(QHash<QString ,QString> data);
    void start_convert();

    int getState() const;
    void setState(int value);

    enum states { waiting=0, converting=1, converter_ok=2, converter_error=3 };

signals:
    void finish();

private:
    void load_data(QHash<QString ,QString> data);
    void insert();
    void update();

private slots:
    void finished(int finish_code);
    void progressRefreshed(int progress);
};

#endif // MEDIA_H
