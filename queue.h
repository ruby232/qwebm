#ifndef QUEUE_H
#define QUEUE_H

#include <QQueue>
#include <QTime>

#include "media.h"

class Queue : public QObject
{
    Q_OBJECT
    QQueue <Media*> medias;
    int processing;
    int max_threads;


public:
    explicit Queue(QObject *parent = 0);
    void enqueue(Media* media);
    Media *dequeue();
    void enqueue(QHash<QString ,QString> data);
    void start();

private:
    void load_db();
    void init_media();

signals:

public slots:

private slots:
    void check_convertion();
    void finish_media();

};

#endif // QUEUE_H
