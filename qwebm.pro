QT += core sql
QT -= gui

TARGET = qwebm
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    converter/ffmpeginterface.cpp \
    converter/mediaconverter.cpp \
    converter/exepath.cpp \
    converter/mediaprobe.cpp \
    scon.cpp \
    media.cpp \
    queue.cpp \
    sqlite/bdsqlite.cpp

INCLUDEPATH += .

HEADERS += \
    converter/ffmpeginterface.h \
    converter/mediaconverter.h \
    converter/exepath.h \
    converter/mediaprobe.h \
    scon.h \
    media.h \
    queue.h \
    sqlite/bdsqlite.h

unix {
    # If DATA_PATH is set, QWinFF searches data in DATA_PATH
    # Otherwise, it uses the executable path as data path.
    DEFINES += DATA_PATH=$(DATA_PATH)
}

# This string is shown in the about box.
DEFINES += VERSION_ID_STRING=$(VERSION_ID_STRING)

# External Short Blocking Operation Timeout
DEFINES += OPERATION_TIMEOUT=30000
DEFINES += DEFAULT_THREAD_COUNT=1

DISTFILES += \
    qwebm.conf
