#ifndef SCON_H
#define SCON_H
#include <QObject>

#include "queue.h"

//Servidor de convertir medias
class SCon: public QObject
{
    Q_OBJECT
    Queue *queue_convertion;

public:
    explicit SCon(QObject *parent = 0);
    void addMedia(QString m_input,QString folder_output="");
    void start();

private:
    void load_config();

signals:

public slots:
};

#endif // SCON_H
