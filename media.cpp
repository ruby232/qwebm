#include "media.h"

Media::Media(QString source, QString m_output, QString error, int state)
{
    this->source = source;
    this->destination = m_output;
    this->error = error;
    this->state = state;
}

Media::Media(int id)
{
    SqliteBD *db = SqliteBD::getIntancia();
    QString query = QString("SELECT * FROM media WHERE id=%1;").arg(id);

    QList<QHash<QString,QString> > result = db->query_select(query);

    if (result.count()>0) {
        QHash<QString ,QString> datos_media = result.at(0);
        this->load_data(datos_media);
    }
}

Media::Media(QString source, QString folder_output)
{
    this->source = source;
    this->error = "";
    this->state = Media::waiting;

    if(folder_output.isEmpty()){
        folder_output = qApp->property("folder_output").toString();
    }

    extension = qApp->property("extension").toString();

    QFileInfo media_info(source);

    if(media_info.exists()){
        QString name = media_info.baseName();
        this->destination = folder_output + name +"." + extension;
        this->insert();
    }else{
        qDebug() << QString("Media %1 no existe").arg(source);
    }
}

Media::Media(QHash<QString, QString> data)
{
    this->load_data(data);
}

int Media::getState() const
{
    return state;
}

void Media::setState(int value)
{
    state = value;

    SqliteBD *db = SqliteBD::getIntancia();
    QString query = QString("UPDATE media set state = %1 WHERE id=%2;")
            .arg(this->state)
            .arg(this->id);
    db->query(query);
}

void Media::insert()
{
    SqliteBD *db = SqliteBD::getIntancia();
    QString query  =  "CREATE TABLE IF NOT EXISTS  'main'.'media' ("
                      "'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                      "'source' TEXT NOT NULL,"
                      "'destination' TEXT NOT NULL,"
                      "'state' INTEGER NOT NULL,"
                      "'error' TEXT NOT NULL,"
                      "'progress' TEXT"
                      " );";

    bool result = db->query(query);
    if(result){

        query = QString("INSERT INTO media (source , destination,state,error,progress) VALUES('%1','%2',%3,'%4','-1');")
                .arg(this->source)
                .arg(this->destination)
                .arg(this->state)
                .arg(this->error);

        result = db->query(query);
        if(result){
            //obtener el id con que se guardo en la base de datos
            query =  "SELECT MAX(id) AS id FROM media";
            QList<QHash<QString,QString> > result = db->query_select(query);
            if (result.count() > 0) {
                this->id = result.at(0).value("id").toInt();
            }
        }
    }
}

void Media::update()
{
    SqliteBD *db = SqliteBD::getIntancia();

    QString query = QString("UPDATE media set source = '%1', destination = '%2',state = %3 ,error ='%4' WHERE id=%5;")
            .arg(this->source)
            .arg(this->destination)
            .arg(this->state)
            .arg(this->error)
            .arg(this->id);

    db->query(query);
}

void Media::start_convert()
{
    converter = new MediaConverter(this->source,this->destination);
    connect(converter,SIGNAL(finished(int)),this,SLOT(finished(int)));
    connect(converter,SIGNAL(progressRefreshed(int)),this,SLOT(progressRefreshed(int)));
    converter->start();
}

void Media::load_data(QHash<QString, QString> data)
{
    this->id = data["id"].toInt();
    this->source = data["source"];
    this->destination = data["destination"];
    this->error = data["error"];
    this->state = data["state"].toInt();
}

void Media::finished(int finish_code)
{
    this->state = Media::converter_ok;
    if(finish_code){
        this->error = converter->errorMessage();
        this->state = Media::converter_error;
    }
    this->update();

    emit(finish());
}

void Media::progressRefreshed(int progress)
{
    SqliteBD *db = SqliteBD::getIntancia();
    QString query = QString("UPDATE media set progress = %1 WHERE id=%2;")
            .arg(progress)
            .arg(this->id);
    db->query(query);
}
