#include "queue.h"

Queue::Queue(QObject *parent) : QObject(parent)
{
    this->max_threads = 4;
    this->start();
}

void Queue::enqueue(Media *media)
{
    this->medias.enqueue(media);
}

Media *Queue::dequeue()
{
    Media *first_media = this->medias.dequeue();
    first_media->setState(Media::converting);
   return first_media;
}

void Queue::enqueue(QHash<QString, QString> data)
{
    Media *media_aux = new Media(data);
    this->enqueue(media_aux);
}

void Queue::start()
{
    this->init_media();
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(check_convertion()));
    timer->start(1000);
}

void Queue::load_db()
{
    SqliteBD *db = SqliteBD::getIntancia();
    QString query = "SELECT * FROM media WHERE state=0;";

    QList<QHash<QString,QString> > result = db->query_select(query);

    this->medias.clear();

    if (result.count()>0) {
        for (int i = 0; i < result.count(); ++i) {
            QHash<QString ,QString> datos_media = result.at(i);
            this->enqueue(datos_media);
        }
    }
}

void Queue::init_media()
{
    this->load_db();

    if(!this->medias.empty()){
        Media *media_conv = this->dequeue();

        connect(media_conv,SIGNAL(finish()),this,SLOT(finish_media()));
        processing++;
        media_conv->start_convert();
    }
}

void Queue::check_convertion()
{
    if(processing < max_threads){
        init_media();
    }
}

void Queue::finish_media()
{
    processing--;
}
