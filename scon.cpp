#include "scon.h"

SCon::SCon(QObject *parent)
{
    this->load_config();
    this->queue_convertion = new Queue(this);
    //this->addMedia("/home/xam/Vídeos/Convertir/aDSCI9861 (1).AVI");
    this->addMedia("/home/xam/Vídeos/Convertir/big-buck-bunny_trailer.webm");
    this->addMedia("/home/xam/Vídeos/Convertir/El_don.wmv");
    this->addMedia("/home/xam/Vídeos/Convertir/Las reglas del metal - Tv de Cierto Pelo--z-EuLXs2GE.mp4");

}

void SCon::addMedia(QString m_input, QString folder_output)
{
    Media* media = new Media(m_input,folder_output);
    queue_convertion->enqueue(media);
}

void SCon::start()
{
    this->queue_convertion->start();
}

void SCon::load_config()
{
    QString params = "-codec:v libvpx-vp9 -quality good -cpu-used 0 -b:v 500k -qmin 10 -qmax 42 -maxrate 500k "
                     "-bufsize 1000k -threads 4 -vf scale=-1:480 -codec:a libvorbis -b:a 128k";
    QString extension = "webm";
    QString folder_output = "/home/xam/Vídeos/Convertidos/";

    QString path_conf="/etc/qwebm/qwebm.conf";
    if(QFile::exists(path_conf)){
        QFile loadFile(path_conf);
        if (!loadFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open save file.");
        }

        QByteArray data = loadFile.readAll();
        QJsonDocument json_doc(QJsonDocument::fromJson(data));
        QJsonObject json_object = json_doc.object();

        QString params_aux = json_object["params"].toString();
        QString extension_aux = json_object["extension"].toString();
        QString folder_output_aux = json_object["folder_output"].toString();

        if(!params_aux.isEmpty()){
            params = params_aux;
        }

        if(!folder_output_aux.isEmpty()){
            folder_output = folder_output_aux;
        }

        if(!extension_aux.isEmpty()){
            extension = extension_aux;
        }
    }

    qApp->setProperty("params",params);
    qApp->setProperty("extension",extension);
    qApp->setProperty("folder_output",folder_output);
}
