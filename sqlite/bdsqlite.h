#ifndef BDSQLITE_H
#define BDSQLITE_H

#include <QtSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QDir>
#include <QFile>
#include <QList>
#include <QHash>
#include <QDebug>


class SqliteBD
{
private:
     //parametro estatico para implementar el patron singleton
    static SqliteBD* instanceSqlite;
    //direccion completa del fichero sqlite donde se encontrar la base de datos
    QString pathBD;
    QString error;
    QString status;
    QSqlDatabase db;

    SqliteBD(QString pathBD="");
public:

   static SqliteBD* getIntancia(QString path_db = "");
    //retorna si existe el fichero entrado por parametros sino se compra
    //con la direccionBD
    bool existBD(QString direccion="");
    //ejecuta consulta que no devuelven datos cono insertar eliminar actualizar
    bool query(QString consulta);
    //retorna una lista de qhash donde cada hash represenata una tupla enla base de datos
    QList<QHash<QString,QString> > query_select(QString consulta);

    //retorna si existe la tabla dada
    bool exist_table(QString tabla);
};

#endif // BDSQLITE_H
