#include "bdsqlite.h"

SqliteBD::SqliteBD(QString direccionBD)
{
    if (direccionBD == "") {
        //this->pathBD = QDir::currentPath() + "/qwebm_queue.sqlite";
        this->pathBD = "/home/xam/tmp/debug-qwebm-Desktop-Release/queue.sqlite";
    }else
    {
        this->pathBD = direccionBD;
    }

   db = QSqlDatabase::addDatabase("QSQLITE");
}

SqliteBD* SqliteBD::instanceSqlite = 0;// Inicializar el puntero

SqliteBD* SqliteBD::getIntancia(QString path_db)
{
    if (instanceSqlite == 0)  // ¿Es la primera llamada?
      {
          instanceSqlite = new SqliteBD(path_db);// Creamos la instancia
      }

      return instanceSqlite; // Retornamos la dirección de la instancia
}

bool SqliteBD::existBD(QString path)
{
    QFile file_aux(pathBD);
    if (path == "") {
        file_aux.setFileName(pathBD);
    }
    return file_aux.exists();
}

bool SqliteBD::query(QString string_query)
{
     qDebug() << string_query;

    db.setDatabaseName(pathBD);

    bool result= true;
    if(!db.open() )
      {
          result =  false;
          error = db.lastError().text();
          status = "Fallo la conección to connect." ;
      }

      status = "Conctado" ;

      QSqlQuery qry;

      qry.prepare(string_query);
      if( !qry.exec() )
      {
          result =  false;
          error = qry.lastError().text();
      }

      db.close();
      QSqlDatabase::removeDatabase("QSQLITE");
      return result;
}

QList<QHash<QString,QString> > SqliteBD::query_select(QString string_query)
{
    error = "";

    db.setDatabaseName(pathBD);

    QList<QHash<QString,QString> > result;

    if(!db.open() )
      {
        error = db.lastError().text();
        status = "Fallo la conección to connect." ;
      }

      status = "Conctado" ;

      QSqlQuery qry;

      qry.prepare(string_query);
      if( !qry.exec() )
      {
        error = qry.lastError().text();
      }

      QSqlRecord rec = qry.record();

      int cols = rec.count();

          for( int r=0; qry.next(); r++ )
          {
              QHash<QString,QString> tuplaAux;
            for( int c=0; c<cols; c++ )
            {
                QString nombreCoumna = rec.fieldName(c);
                QString valor = qry.value(c).toString();
                tuplaAux[nombreCoumna] = valor;
            }
            result.append(tuplaAux);
          }

          status = "Exitoso";

      db.close();
      return result;

}
//retorna si existe la tabla dada
bool SqliteBD::exist_table(QString tabla)
{
    QString string_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+tabla+"';";
    QList<QHash<QString,QString> > result_query = query_select(string_query);

    bool result =true;
    if (result_query.count() == 0) {
        result =false;
    }

    return result;
}
