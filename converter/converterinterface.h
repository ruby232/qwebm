#ifndef CONVERTERINTERFACE_H
#define CONVERTERINTERFACE_H



/**
  Abstract Converter Interface
  Derived class must implement the virtual functions to provide converter details.
*/

class ConverterInterface : public QObject
{
    Q_OBJECT
public:
    explicit ConverterInterface(QObject *parent = 0,QString source="", QString destination="");
    virtual QString executableName() const = 0;
    virtual void reset() = 0;
    virtual QProcess::ProcessChannel processReadChannel() const = 0;
    virtual void fillParameterList() = 0;
    virtual void parseProcessOutput(const QString& line) = 0;
    virtual double progress() const = 0;
    virtual QString errorMessage() const;


signals:
    void progressRefreshed(double percentage);

public slots:

};

#endif // CONVERTERINTERFACE_H
