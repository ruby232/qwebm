#include "ffmpeginterface.h"
#include "mediaprobe.h"
#include "exepath.h"
#include <QRegExp>
#include <QTextStream>
#include <QDebug>
#include <QAtomicInt>
#include <cmath>

// timeout before force-terminating ffmpeg
#ifdef OPERATION_TIMEOUT
#   define TIMEOUT OPERATION_TIMEOUT
#else
#   define TIMEOUT 3000
#endif

namespace {
namespace patterns {
const char progress[]
= "size=\\s*([0-9]+)kB\\s+time=\\s*([0-9]+\\.[0-9]+)\\s+bitrate=\\s*([0-9]+\\.[0-9]+)kbits/s";
enum Progress_1_Fields {
    PROG_1_TIME = 2
};

const char progress2[] // another possible format where time is represented as hh:mm:ss
= "size=\\s*([0-9]+)kB\\s+time=\\s*([0-9][0-9]):([0-9][0-9]):([0-9][0-9](\\.[0-9][0-9]?)?)\\s+"
  "bitrate=\\s*([0-9]+\\.[0-9]+)kbits/s";
enum Progress_2_Fields {
    PROG_2_HR = 2,
    PROG_2_MIN,
    PROG_2_SEC
};

const char duration[]
= "Duration:\\s+([0-9][0-9]):([0-9][0-9]):([0-9][0-9](\\.[0-9][0-9]?)?)";
} // namespace patterns

namespace info {
QAtomicInt is_encoders_read(false);
volatile bool ffmpeg_exist = false;
QString ffmpeg_version;
QString ffmpeg_codec_info;
QString ffmpeg_format_info;
QList<QString> video_encoders;
QList<QString> muxing_formats;
QList<QString> demuxing_formats;

namespace inner {

/**
     * @brief Extract encoder information from codec description.
     * @param target Extracted encoder names will be pushed into @a target
     * @param s the codec description string
     * @return number of encoders found
     */
int find_encoders_in_desc(QStringList& target, const QString& s) {
    const char *keyword_begin = "(encoders:";
    const char *keyword_end = ")";
    int begin = s.indexOf(keyword_begin);
    if (begin < 0)
        return 0; // encoder name not found in description
    begin += strlen(keyword_begin);
    int end = s.indexOf(keyword_end, begin);
    if (end < 0)
        return 0; // error, mission ')'
    int length = end - begin;

    // encoder_str contains encoder names separated by spaces, and
    // may contain leading and trailing spaces.
    QString encoders_str = s.mid(begin, length);

    // split encoder_str into encoder names and skip whitespaces
    QStringList encoders = encoders_str.split(' ', QString::SkipEmptyParts);
    foreach (QString s, encoders) {
        target.push_back(s); // fill codec names into the list
    }

    return encoders.size();
}

bool read_ffmpeg_codecs(const char *flag)
{
    QProcess ffmpeg_process;
    QStringList parameters;
    parameters.push_back(QString(flag));

    ffmpeg_process.setReadChannel(QProcess::StandardOutput);

    //qDebug() << ExePath::getPath("ffmpeg") << parameters.join(" ");
    ffmpeg_process.start(ExePath::getPath("ffmpeg"), parameters);

    // Wait until ffmpeg has started.
    if (!ffmpeg_process.waitForStarted(TIMEOUT)) {
        return false;
    }

    // Wait until ffmpeg has finished.
    if (!ffmpeg_process.waitForFinished(TIMEOUT)) {
        return false;
    }

    if (ffmpeg_process.exitCode() != 0) {
        return false; // error
    }

    // Find all available encoders
    QRegExp pattern("[ D]E([ VAS])...\\s+([^ ]+)\\s*(.*)$");
    QStringList encoder_list; // temporary storage of encoder names
    const int AV_INDEX = 1;
    const int CODEC_NAME_INDEX = 2;
    const int CODEC_DESC = 3;

    ffmpeg_codec_info.clear();
    while (ffmpeg_process.canReadLine()) {
        QString line(ffmpeg_process.readLine());
        ffmpeg_codec_info.append(line);

        if (pattern.indexIn(line) != -1) {
            QString av = pattern.cap(AV_INDEX);
            QString codec = pattern.cap(CODEC_NAME_INDEX);
            QString desc = pattern.cap(CODEC_DESC);

            // extract codec names
            encoder_list.clear();
            if (!find_encoders_in_desc(encoder_list, desc))
                encoder_list.push_back(codec);

            foreach (QString codec_name, encoder_list) {
                if (av == "V") { // video encoder
                    //qDebug() << "Video Codec: " + codec_name;
                    video_encoders.push_back(codec_name);
                }
            }
        }
    }

    return true;
}

bool read_ffmpeg_version()
{
    QProcess ffmpeg_process;
    QStringList parameters;
    parameters.push_back(QString("-version"));

    //qDebug() << ExePath::getPath("ffmpeg") << parameters.join(" ");
    ffmpeg_process.start(ExePath::getPath("ffmpeg"), parameters);

    ffmpeg_process.waitForStarted(TIMEOUT);
    ffmpeg_process.waitForFinished(TIMEOUT);
    ffmpeg_version = QString(ffmpeg_process.readAll());

    return true;
}

bool read_ffmpeg_formats()
{
    QProcess ffmpeg_process;
    QStringList parameters;
    parameters << "-formats";

    ffmpeg_process.start(ExePath::getPath("ffmpeg"), parameters);

    ffmpeg_process.waitForStarted(TIMEOUT);
    ffmpeg_process.waitForFinished(TIMEOUT);
    if (ffmpeg_process.exitCode() != 0)
        return false;

    muxing_formats.clear();
    demuxing_formats.clear();
    ffmpeg_format_info.clear();

    QRegExp pattern("^ ([ D])([ E]) ([^ ]+)\\s+(.*)$");
    const int INDEX_DEMUX = 1;
    const int INDEX_MUX = 2;
    const int INDEX_NAME = 3;
    //const int INDEX_DETAIL = 4;

    while (ffmpeg_process.canReadLine()) {
        QString line(ffmpeg_process.readLine());
        ffmpeg_format_info.append(line);
        if (pattern.indexIn(line) != -1) {
            QString name = pattern.cap(INDEX_NAME);
            if (pattern.cap(INDEX_DEMUX) == "D")
                demuxing_formats.append(name);
            if (pattern.cap(INDEX_MUX) == "E")
                muxing_formats.append(name);
        }
    }

    return true;
}

} // namespace inner

/* Read FFmpeg information.
     *  (1) Check available encoder from "ffmpeg -codecs" and "ffmpeg -formats".
     *  (2) Read ffmpeg version information by "ffmpeg -version" command.
     * Once the information is correctly read, it never
     * execute ffmpeg to acquire the information again.
    */
void read_ffmpeg_info()
{
    if (!is_encoders_read.testAndSetAcquire(false, true))
        return; // Skip the operation if the information was already read.

    qDebug() << "Read FFmpeg Information";

    /* Older versions of ffmpeg has no "-codecs" flag and report all
         * supported codecs by "-formats". Recent versions report codecs
         * by "-codecs" flag, so we check "-codecs" first. If ffmpeg
         * returns an error, retry with flag "-formats".
         */
    if (!inner::read_ffmpeg_codecs("-codecs") && !inner::read_ffmpeg_codecs("-formats")) {
        is_encoders_read = false; // allow retry when failed
        return;
    }

    if (!inner::read_ffmpeg_version())
        return;

    if (!inner::read_ffmpeg_formats())
        return;

    ffmpeg_exist = true;
}

} // namespace info

// extract error message from the line
QString extract_errmsg(const QString& line)
{
    QRegExp pattern("^[^:]*:(.*)$");
    const int INDEX_MESSAGE = 1;
    if (pattern.indexIn(line) != -1)
        return pattern.cap(INDEX_MESSAGE).trimmed();
    else
        return "";
}

// scale sec with speed_factor if scale == true
double scale_time(unsigned int sec, bool scale, double speed_factor)
{
    if (!sec)
        return 0;
    else if (scale)
        return sec / speed_factor; // (speed *= 2) means (time /= 2)
    else
        return sec;
}

} // anonymous namespace

struct FFmpegInterface::Private
{
    double duration;
    double progress;
    QString stringBuffer;
    QRegExp progress_pattern;
    QRegExp progress_pattern_2;
    QRegExp duration_pattern;

    bool encoders_read;
    QList<QString> video_encoders;

    QString errmsg;

    Private() : duration(0), progress(0)
      , progress_pattern(patterns::progress)
      , progress_pattern_2(patterns::progress2)
      , duration_pattern(patterns::duration)
      , encoders_read(false) { }

    bool check_progress(const QString&);
    QStringList getOptionList(QString source, QString destination,bool*);
};

/*! Check whether the output line is a progress line.
    If it is, update p->progress and return true.
    Otherwise, keep the value of p->progress and return false.
*/
bool FFmpegInterface::Private::check_progress(const QString& line)
{
    QRegExp& pattern = progress_pattern;
    int index = pattern.indexIn(line);
    if (index != -1) {
        const double t = pattern.cap(patterns::PROG_1_TIME).toDouble();

        // calculate progress
        progress = (t / duration) * 100;

        return true;
    } else { // try another pattern
        QRegExp& alternate_pattern = progress_pattern_2;
        if (alternate_pattern.indexIn(line) != -1) {
            const int hour = alternate_pattern.cap(patterns::PROG_2_HR).toInt();
            const int min = alternate_pattern.cap(patterns::PROG_2_MIN).toInt();
            const double sec = alternate_pattern.cap(patterns::PROG_2_SEC).toDouble();
            const double t = hour*3600 + min*60 + sec;

            progress = (t / duration) * 100;

            return true;
        }
    }
    errmsg = line; // save the last output line
    return false;
}

/**
 * Set a ffmpeg command-line option list.
 * @param source and destination file, QString
 * @return a QStringList containing command-line options
 * @note This function is time-consuming because it calls ffprobe to obtain
 *       media information.
 */
QStringList FFmpegInterface::Private::getOptionList(QString source, QString destination,bool *success)
{
    MediaProbe probe;

    /*-codec:v
     *h264 -> h264
     *libx264 -> h264
     *libvpx -> VP8
     *libvpx-vp9 -> VP9
     */
    /*
     * -quality
     * best, good ,realtime.
    */

    if (!probe.run(source, TIMEOUT)) {
        if (success)
            *success = false;
        return QStringList();
    }

    QStringList list;

    // overwrite if file exists
    list.append("-y");
    /* in this configuration, input is read from file
           arguments: -i <infile>
    */
    list << "-i" << source;

    QString params = qApp->property("params").toString();

    list.append(params.split(" "));

    // set format webm
    list << "-f" << "webm";

    // destination file
    list.append(destination);

    // record duration
    duration = probe.mediaDuration();

    if (success)
        *success = true;
    return list;
}

FFmpegInterface::FFmpegInterface(QObject *parent,QString source, QString destination) :
    p(new Private)
{
    this->source = source;
    this->desination = destination;
}

FFmpegInterface::~FFmpegInterface()
{
    delete p;
}

// virtual functions
QString FFmpegInterface::executableName() const
{
    return ExePath::getPath("ffmpeg");
}

void FFmpegInterface::reset()
{
    p->duration = 0;
    p->progress = 0;
    p->stringBuffer.clear();
    p->errmsg.clear();
}

QProcess::ProcessChannel FFmpegInterface::processReadChannel() const
{
    return QProcess::StandardError;
}

void FFmpegInterface::fillParameterList(QString source, QString destination,QStringList &list)
{
    this->source = source;
    this->desination = destination;
    bool success; // TODO: return success
    list = p->getOptionList(this->source,this->desination,&success);
}

void FFmpegInterface::parseProcessOutput(const QString &data)
{
    //qDebug() << data;

    // split incoming data by [end of line] or [carriage return]
    QStringList lines(data.split(QRegExp("[\r\n]"), QString::KeepEmptyParts));

    if (!p->stringBuffer.isEmpty()) { // prepend buffered data
        lines.front().prepend(p->stringBuffer);
        p->stringBuffer.clear();
    }

    if (!lines.back().isEmpty()) { // buffer incomplete data
        p->stringBuffer = lines.back();
        lines.back().clear();
    }

    QStringList::iterator it = lines.begin();

    for (; it!=lines.end(); ++it) { // parse lines
        QString& line = *it;
        if (line.isEmpty()) continue;
        if (p->check_progress(line)) {
            emit progressRefreshed(p->progress);
            continue;
        }
    }
}

double FFmpegInterface::progress() const
{
    return p->progress;
}

QString FFmpegInterface::errorMessage() const
{
    return extract_errmsg(p->errmsg);
}

bool FFmpegInterface::getVideoEncoders(QList<QString> &target)
{
    info::read_ffmpeg_info();
    if (!info::ffmpeg_exist) return false;

    target = info::video_encoders;
    return true;
}

bool FFmpegInterface::getVideoEncoders(QSet<QString> &target)
{
    QList<QString> encoder_list;
    if (!getVideoEncoders(encoder_list))
        return false;
    target = QSet<QString>::fromList(encoder_list);
    return true;
}

QString FFmpegInterface::getFFmpegVersionInfo()
{
    info::read_ffmpeg_info();
    return info::ffmpeg_version;
}

QString FFmpegInterface::getFFmpegCodecInfo()
{
    info::read_ffmpeg_info();
    return info::ffmpeg_codec_info;
}

QString FFmpegInterface::getFFmpegFormatInfo()
{
    info::read_ffmpeg_info();
    return info::ffmpeg_format_info;
}

bool FFmpegInterface::getSupportedMuxingFormats(QSet<QString> &target)
{
    info::read_ffmpeg_info();
    if (!info::ffmpeg_exist) return false;

    target = QSet<QString>::fromList(info::muxing_formats);
    return true;
}

bool FFmpegInterface::getSupportedDemuxingFormats(QSet<QString> &target)
{
    info::read_ffmpeg_info();
    if (!info::ffmpeg_exist) return false;

    target = QSet<QString>::fromList(info::demuxing_formats);
    return true;
}

bool FFmpegInterface::hasFFmpeg()
{
    info::read_ffmpeg_info();
    return info::ffmpeg_exist;
}

void FFmpegInterface::refreshFFmpegInformation()
{
    info::is_encoders_read = false;
    info::read_ffmpeg_info();
}

QString FFmpegInterface::getSource() const
{
    return source;
}

void FFmpegInterface::setSource(const QString &value)
{
    source = value;
}

QString FFmpegInterface::getDesination() const
{
    return desination;
}

void FFmpegInterface::setDesination(const QString &value)
{
    desination = value;
}
