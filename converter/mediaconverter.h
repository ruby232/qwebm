#ifndef MEDIACONVERTER_H
#define MEDIACONVERTER_H

#include <QObject>
#include <QProcess>
#include "ffmpeginterface.h"


class MediaConverter : public QObject
{
    Q_OBJECT
public:
    explicit MediaConverter(QString m_inputFileName, QString m_outputFileName, QObject *parent = 0);
    ~MediaConverter();

    /*!
     * Start the conversion process.
     * @return If the process is successfully started, the function returns true
     *  Otherwise, the function returns false.
     */
    bool start();

    void stop();
    double progress();

    /*!
     * Check whether required external programs (e.g. ffmpeg)
     * is available.
     * @param msg [out] Output message if an error occurs.
     * @return true if all dependencies are met, false otherwise.
     */
    static bool checkExternalPrograms(QString &msg);

    /*!
     * Get the error message if the conversion fails.
     * @return the last output line ffmpeg prints, likely to be
     *         an error message
     */
    QString errorMessage() const;

signals:
    void finished(int exitcode);
    void progressRefreshed(int percentage);

public slots:

private slots:
    void readProcessOutput();
    void convertProgressFinished(int, QProcess::ExitStatus);
    void convertProgressRefreshed(double);

private:
    Q_DISABLE_COPY(MediaConverter)
    QProcess m_proc;
    FFmpegInterface *m_pConv;
    QString m_outputFileName;
    QString m_inputFileName;
    QString m_tmpFileName;
    bool m_stopped;
};

#endif // MEDIACONVERTER_H
