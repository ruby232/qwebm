#ifndef FFMPEGINTERFACE_H
#define FFMPEGINTERFACE_H
#include <QStringList>
#include <QProcess>
#include <QObject>
#include <QList>
#include <QSet>
#include <QCoreApplication>

class FFmpegInterface : public QObject
{
    Q_OBJECT
public:
    explicit FFmpegInterface(QObject *parent = 0, QString source="", QString destination="");
    virtual ~FFmpegInterface();
    QString executableName() const;
    void reset();
    QProcess::ProcessChannel processReadChannel() const;
    void parseProcessOutput(const QString& data);
    double progress() const;
    QString errorMessage() const;

    static bool getVideoEncoders(QList<QString>& target);
    static bool getVideoEncoders(QSet<QString>& target);
    static QString getFFmpegVersionInfo();
    static QString getFFmpegCodecInfo();
    static QString getFFmpegFormatInfo();
    static bool getSupportedMuxingFormats(QSet<QString>& target);
    static bool getSupportedDemuxingFormats(QSet<QString>& target);
    static bool hasFFmpeg();

    static void refreshFFmpegInformation();

    QString getSource() const;
    void setSource(const QString &value);

    QString getDesination() const;
    void setDesination(const QString &value);

    void fillParameterList(QString source, QString destination, QStringList &list);
signals:
    void progressRefreshed(double percentage);

public slots:

private:
    Q_DISABLE_COPY(FFmpegInterface)
    struct Private;
    Private *p;
    QString source;
    QString desination;
};

#endif // FFMPEGINTERFACE_H
