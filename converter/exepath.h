#ifndef EXEPATH_H
#define EXEPATH_H

#include <QString>
#include <QList>

class ExePath
{
public:
    static void setPath(QString program, QString path);
    static QString getPath(QString program);

    /**
     * @brief Check whether the program can be executed.
     * @param program the program to test
     */
    static bool checkProgramAvailability(QString program);

    /**
     * Save the paths using QSettings
     */
    static void saveSettings();

    /**
     * Load the paths using QSettings
     */
    static void loadSettings();

    static QList<QString> getPrograms();

private:
    ExePath();
};

#endif // EXEPATH_H
